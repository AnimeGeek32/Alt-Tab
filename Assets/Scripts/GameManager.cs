﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace AltTab
{
    using System.Collections.Generic;       //Allows us to use Lists. 
    using UnityEngine.UI;                   //Allows us to use UI.

    public class GameManager : MonoBehaviour
    {
        public Camera[] cameras;
        public Image boss;
		public Image darkOverlay;
		public Image waitingBoss;
        public Text mainTitle;
		public Text creditTitle;

		public float maxTimeBossIsAway = 5; // set duration time in seconds in the Inspector
        public float bossComingTimer = 5; // set duration time in seconds in the Inspector
        public float bossWaitingTimer = 10; // set duration time in seconds in the Inspector
        public float gameRestartInSecs = 7.0f;
        public bool platformerPlayerInputEnabled = false;

        private int currentCameraIndex;

		private float defaultMaxTimeBossIsAway;
        private float defaultBossComingTimer;
        private float defaultBossWaitingTimer;

        private bool didGameStart = false;
        private bool isGameOver = false;
        private bool isBossWatching = false;
        private bool winConditionIsMet = false;

        private bool isWorkDone = false;
		private bool isTabDown = false;

        public float levelStartDelay = 2f;                      //Time to wait before starting level, in seconds.
        public float turnDelay = 0.1f;                          //Delay between each Player turn.

        [FMODUnity.EventRef]
        public string spreadsheetSnapshotEvent;
        FMOD.Studio.EventInstance spreadsheetSnapshot;
        [FMODUnity.EventRef]
        public string platformerSnapshotEvent;
        FMOD.Studio.EventInstance platformerSnapshot;
		[FMODUnity.EventRef]
		public string tabSoundEvent;
		[FMODUnity.EventRef]
		public string bossFootstepsEvent;
		FMOD.Studio.EventInstance bossFootsteps;
		[FMODUnity.EventRef]
		public string bossFiresYouEvent;
		[FMODUnity.EventRef]
		public string officeAmbientSoundEvent;
		FMOD.Studio.EventInstance officeAmbientSound;
		[FMODUnity.EventRef]
		public string typingSoundEvent;
		FMOD.Studio.EventInstance typingSound;

        public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.

        //Awake is always called before any Start functions
        void Awake()
        {
			Debug.Log("AWAKE GAME!");
            //Check if instance already exists
            if (instance == null)
                instance = this;

            //Replace current instance with new one
            else if (instance != this)
            {
                Destroy(instance.gameObject);
                instance = null;
                instance = this;
            }

            //Sets this to not be destroyed when reloading scene
            //DontDestroyOnLoad(gameObject);

        }

        void Start()
        {

			Debug.Log("START GAME.");

            //setup the UI
            boss.enabled = false;
			waitingBoss.enabled = false;
			darkOverlay.enabled = true;
			creditTitle.enabled = true;

			defaultMaxTimeBossIsAway = maxTimeBossIsAway;
            defaultBossComingTimer = bossComingTimer;
            defaultBossWaitingTimer = bossWaitingTimer;
				
            mainTitle.GetComponent<Text>().text = "Alt-Tab To Start";
            mainTitle.enabled = true;

            currentCameraIndex = 0;

            //Turn all cameras off, except the first default one
            for (int i = 1; i < cameras.Length; i++)
            {
                cameras[i].enabled = false;
            }

            //If any cameras were added to the controller, enable the first one
            if (cameras.Length > 0)
            {
                cameras[0].enabled = true;
                Debug.Log("Camera with name: " + cameras[0].GetComponent<Camera>().name + ", is now enabled");
            }

            spreadsheetSnapshot = FMODUnity.RuntimeManager.CreateInstance(spreadsheetSnapshotEvent);
			spreadsheetSnapshot.start();

            platformerSnapshot = FMODUnity.RuntimeManager.CreateInstance(platformerSnapshotEvent);

			bossFootsteps = FMODUnity.RuntimeManager.CreateInstance (bossFootstepsEvent);
			bossFootsteps.start();
			//bossFootsteps.setVolume(0.0f);

			officeAmbientSound = FMODUnity.RuntimeManager.CreateInstance (officeAmbientSoundEvent);
			officeAmbientSound.start();

			typingSound = FMODUnity.RuntimeManager.CreateInstance (typingSoundEvent);
			//officeAmbientSound.setVolume(0.2f);

        }

        //Update is called every frame.
        void Update()
        {
			//BOSS CHECK

			//BOSS IS AWAY
			if (maxTimeBossIsAway > 0 && didGameStart == true) 
			{
				maxTimeBossIsAway -= Time.deltaTime; // I need timer which from a particular time goes to zero				
				Debug.Log("BOSS IS AWAY FOR " + maxTimeBossIsAway + " sec");

			} else if (maxTimeBossIsAway <= 0) {

				//BOSS IS COMING
	            if (bossComingTimer > 0 && didGameStart == true && isBossWatching == false)
	            {
	                bossComingTimer -= Time.deltaTime; // I need timer which from a particular time goes to zero				
					bossFootsteps.setParameterValue ("Boss Distance", bossComingTimer);
					Debug.Log("BOSS IS COMING IN " + bossComingTimer + " sec");

	            }
	            else if (didGameStart == true)
	            {
					//YOU'RE FIRED
	                if (currentCameraIndex != 1)
	                {
						Debug.Log("BOSS FOUND YOU!!");
	                    GameOverLoseAtLife();
	                    return;
	                }
	                else if (isBossWatching == false)
	                {
						//BOSS ARRIVES
						waitingBoss.enabled = true;
	                    isBossWatching = true;
	                    Debug.Log("BOSS IS HERE!");

	                }
	                else if (bossWaitingTimer > 0 && isBossWatching == true)
	                {
						//BOSS IS WAITING

	                    bossWaitingTimer -= Time.deltaTime; 
	                    Debug.Log("BOSS IS WAITING FOR REPORT!" + bossWaitingTimer + " sec");

						if (isWorkDone) {
							Debug.Log ("Work is done!");
							bossFootsteps.setParameterValue ("Boss Distance", 7);
							ResetTimers();

                            waitingBoss.enabled = false;
							boss.enabled = false;
							isBossWatching = false;
							isWorkDone = false;
						}
	                }

                    else if (bossWaitingTimer <= 0 && !isWorkDone)
	                {
                        //BOSS Fires Player
                        Debug.Log("BOSS is unhappy with your work progress. He fires you!");
                        GameOverLoseAtLife();
	                }
	            }
			}

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
			if (Input.GetKey(KeyCode.Tab)) {
#else
            if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.Tab)) {
#endif
                //reset game
                if (!isTabDown)
                {
                    isTabDown = true;
                    FMODUnity.RuntimeManager.PlayOneShotAttached(tabSoundEvent, gameObject);

                    if (!isGameOver)
                    {
                        if (didGameStart == false)
                        {
							darkOverlay.enabled = false;
							creditTitle.enabled = false;
                            boss.enabled = false;
                            mainTitle.enabled = false;
                            didGameStart = true;
                        }

                        currentCameraIndex++;
                        Debug.Log("Alt-tab has been pressed. Switching to the next camera of index:" + currentCameraIndex);
                        if (currentCameraIndex < cameras.Length)
                        {
                            cameras[currentCameraIndex - 1].enabled = false;
                            cameras[currentCameraIndex].enabled = true;
                            Debug.Log("Camera with name: " + cameras[currentCameraIndex].GetComponent<Camera>().name + ", is now enabled");
                        }
                        else
                        {
                            cameras[currentCameraIndex - 1].enabled = false;
                            currentCameraIndex = 0;
                            cameras[currentCameraIndex].enabled = true;
                            Debug.Log("Camera with name: " + cameras[currentCameraIndex].GetComponent<Camera>().name + ", is now enabled");
                        }


                        if (currentCameraIndex == 2)
                        {
                            spreadsheetSnapshot.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                            platformerSnapshot.start();
                            platformerPlayerInputEnabled = true;
                        }
                        else
                        {
                            platformerSnapshot.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                            spreadsheetSnapshot.start();
                            platformerPlayerInputEnabled = false;
                        }
                    }
                }
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            } else if (Input.GetKeyUp(KeyCode.Tab)) {
#else
            } else if (Input.GetKeyUp(KeyCode.LeftAlt) || Input.GetKeyUp(KeyCode.Tab)) {
#endif
				isTabDown = false;
			}
		}

        public void WinGame()
        {
            Debug.Log("You win the game!");
            if (!winConditionIsMet)
            {
                winConditionIsMet = true;
				GameOverWin();
                //StartCoroutine(RestartGameInSecs(5.0f));
            }
        }
			
		public void WorkFinished() {
			isWorkDone = true;
		}

		public int GetCurrentCameraIndex() {
			return currentCameraIndex;
		}
							
		public void GameOverLoseAtLife()
		{			
			//Disable this GameManager.
			Debug.Log ("YOU'RE FIRED!!");
			//officeAmbientSound.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
			bossFootsteps.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);

			FMODUnity.RuntimeManager.PlayOneShotAttached(bossFiresYouEvent,gameObject);
			mainTitle.enabled = true;
			mainTitle.GetComponent<Text>().text = "YOU'RE FIRED!!";

			darkOverlay.enabled = true;
			waitingBoss.enabled = false;
			boss.enabled = true;

			didGameStart = false;
			isGameOver = true;

			ResetTimers();
            StartCoroutine(RestartGameInSecs(gameRestartInSecs));
		}
			
		public void GameOverLoseAtGame()
		{			
			//Disable this GameManager.
			Debug.Log ("GAME OVER MAN! GAME OVER!!");

			darkOverlay.enabled = true;
			mainTitle.enabled = true;
			mainTitle.GetComponent<Text>().text = "YOU'VE LOST THE GAME! YOU SUCK!!!";

			didGameStart = false;
			isGameOver = true;
			bossFootsteps.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
			ResetTimers();
            StartCoroutine(RestartGameInSecs(gameRestartInSecs));
		}
			
		public void GameOverWin()
		{			
			//Disable this GameManager.
			Debug.Log ("YOU WIN!!");
			bossFootsteps.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
			mainTitle.GetComponent<Text>().text = "YOU WON THE GAME!!";
			mainTitle.enabled = true;

			darkOverlay.enabled = true;
			waitingBoss.enabled = false;
			boss.enabled = false;

			didGameStart = false;
			isGameOver = true;

			ResetTimers();
            StartCoroutine(RestartGameInSecs(gameRestartInSecs));
		}

		private void ResetTimers() {
			bossComingTimer = defaultBossComingTimer;
			bossWaitingTimer = defaultBossWaitingTimer;
			maxTimeBossIsAway = defaultMaxTimeBossIsAway;
		}

        IEnumerator RestartGameInSecs(float sec)
        {
			
            yield return new WaitForSeconds(sec);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
	}
}

