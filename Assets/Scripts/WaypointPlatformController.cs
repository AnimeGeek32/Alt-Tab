﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointPlatformController : MonoBehaviour {
	public Vector3[] localWaypoints;
	private Vector3[] globalWaypoints;

	public float speed;
	public bool cyclic;
	public float waitTime;

	private int fromWaypointIndex;
	private float percentBetweenWaypoints;
	private float nextMoveTime;

    private MovingPlatformMotor2D _mpMotor;

	// Use this for initialization
	void Start () {
		_mpMotor = GetComponent<MovingPlatformMotor2D>();

		globalWaypoints = new Vector3[localWaypoints.Length];
		for (int i = 0; i < globalWaypoints.Length; i++)
		{
			globalWaypoints[i] = localWaypoints[i] + transform.position;
		}
	}
	
	// Update is called once per frame
	void Update () {
        _mpMotor.velocity = CalculatePlatformMovement();
	}

	private Vector3 CalculatePlatformMovement()
	{
		if (Time.time < nextMoveTime)
		{
			return Vector3.zero;
		}

		fromWaypointIndex %= globalWaypoints.Length;
		int toWaypointIndex = (fromWaypointIndex + 1) % globalWaypoints.Length;

        Vector3 heading = globalWaypoints[toWaypointIndex] - transform.position;
        float resultDistance = heading.sqrMagnitude;

        if (resultDistance <= 0.01f)
		{
			fromWaypointIndex++;

			if (!cyclic)
			{
				if (fromWaypointIndex >= globalWaypoints.Length - 1)
				{
					fromWaypointIndex = 0;
					System.Array.Reverse(globalWaypoints);
				}
			}

			nextMoveTime = Time.time + waitTime;
            return Vector3.zero;
		}

        //Debug.Log("CalculatePlatformMovement: " + resultDistance);
        return heading * speed;
	}

	private void OnDrawGizmos()
	{
		if (localWaypoints != null)
		{
			Gizmos.color = Color.red;
			float size = .3f;

			for (int i = 0; i < localWaypoints.Length; i++)
			{
				Vector3 globalWaypointPos = (Application.isPlaying) ? globalWaypoints[i] : localWaypoints[i] + transform.position;
				Gizmos.DrawLine(globalWaypointPos - Vector3.up * size, globalWaypointPos + Vector3.up * size);
				Gizmos.DrawLine(globalWaypointPos - Vector3.left * size, globalWaypointPos + Vector3.left * size);
			}
		}
	}
}
