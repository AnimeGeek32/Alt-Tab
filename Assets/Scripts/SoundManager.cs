﻿using UnityEngine;
using System.Collections;

namespace AltTab
{
	public class SoundManager : MonoBehaviour 
	{
		[FMODUnity.EventRef]
		public string bossArriveEvent;
		FMOD.Studio.EventInstance bossArrive;
		

		private void Start ()
		{
			bossArrive = FMODUnity.RuntimeManager.CreateInstance (bossArriveEvent);
		}
		//Used to play single sound clips.

		private void Update ()
		{
			bossArrive.start ();
			//bossArrive.setParameterValue ("Distance", bossDistance);
		}
		

	}
}
