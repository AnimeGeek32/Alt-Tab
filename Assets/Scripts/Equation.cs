﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Equation : MonoBehaviour {

	int term1;
	int term2;
	public int answer;
	public string equationString;
	public int maxTermValue;
	public string descriptionString;
	public string titleString;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

	}

	public void newEquation () {
		term1 = Random.Range (2, maxTermValue);
		term2 = Random.Range (2, maxTermValue);
		int opperation = Random.Range (1, 4);
		if (opperation == 1) {
			titleString = "Total Sales";
			descriptionString = string.Format ("Item A sales: {0}, Item B sales: {1}", term1, term2);
			answer = term1 + term2;
			equationString = "Item A + Item B";
		} else if (opperation == 2) {
			titleString = "Quarterly Profit/Loss";
			descriptionString = string.Format ("Total revenue: {0}, Total expenses: {1}", term1, term2);
			answer = term1 - term2;
			equationString = "Total revenue - Total expenses";
		} else {
			titleString = "Revenue Report";
			descriptionString = string.Format ("Total units sold: {0}, Price per unit: {1}", term1, term2);
			answer = term1 * term2;
			equationString = "Total units sold * Price per unit";
		}
	}
}
