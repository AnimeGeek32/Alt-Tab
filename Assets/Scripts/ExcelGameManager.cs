﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AltTab {
	public class ExcelGameManager : MonoBehaviour {

		public Equation equation;
		public Text equationText;
		public Text titleText;
		public Text descriptionText;
		public InputField input;
		public int equationsToSolve;
		private int equationsLeft;
		[FMODUnity.EventRef]
		public string equationCorrectSoundEvent;
		FMOD.Studio.EventInstance equationCorrectSound;
		[FMODUnity.EventRef]
		public string equationWrongSoundEvent;
		FMOD.Studio.EventInstance equationWrongSound;
		[FMODUnity.EventRef]
		public string typingSoundEvent;
		FMOD.Studio.EventInstance typingSound;

		// Use this for initialization
		void Start () {
			equationsLeft = equationsToSolve;
			input.onEndEdit.AddListener (delegate {
				CheckAnswer (input);
			});
			UpdateEquation ();
			equationCorrectSound = FMODUnity.RuntimeManager.CreateInstance (equationCorrectSoundEvent);
			equationWrongSound = FMODUnity.RuntimeManager.CreateInstance (equationWrongSoundEvent);
			typingSound = FMODUnity.RuntimeManager.CreateInstance (typingSoundEvent);
		}

		// Update is called once per frame
		void Update () {
			if (Input.anyKeyDown) {
				typingSound.start ();
				typingSound.release ();
				typingSound = FMODUnity.RuntimeManager.CreateInstance (typingSoundEvent);
			}
				
			if (GameManager.instance.GetCurrentCameraIndex () == 1) {
				if (!input.isFocused && equationsLeft > 0) {
					input.enabled = true;
					input.ActivateInputField ();
					input.Select ();
				}
			} else {
				input.enabled = false;
			}
		}

		void UpdateEquation () {
			equation.newEquation ();
			equationText.text = equation.equationString;
			titleText.text = equation.titleString;
			descriptionText.text = equation.descriptionString;
		}

		void ResetWorkload() {
			equationsLeft = equationsToSolve;
			UpdateEquation ();
		}

		void CheckAnswer(InputField inpt) {
			if (inpt.text == equation.answer.ToString()) {
				equationsLeft -= 1;
				//FMODUnity.RuntimeManager.PlayOneShot (equationCorrectSound);
				equationCorrectSound.start();
				equationCorrectSound.release ();
				equationCorrectSound = FMODUnity.RuntimeManager.CreateInstance (equationCorrectSoundEvent);

				if (equationsLeft > 0) {
					UpdateEquation ();
				} else {
					AltTab.GameManager.instance.WorkFinished ();
					ResetWorkload ();
				}
			} else {
				//FMODUnity.RuntimeManager.PlayOneShot (equationWrongSound);
				equationWrongSound.start();
				equationWrongSound.release();
				Debug.Log ("Wrong Answer");
				equationWrongSound = FMODUnity.RuntimeManager.CreateInstance (equationWrongSoundEvent);

			}
			input.text = "";
		}
	}
}