﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGoalController : MonoBehaviour {
    public bool isTouched = false;

	void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            if (!isTouched)
            {
                collider.gameObject.GetComponent<PlayerController2D>().enabled = false;
                AltTab.GameManager.instance.WinGame();
                isTouched = true;
            }
        }
    }
}
