# Alt-Tab

A game in which you play as an intern trying to sneak minutes on a platformer while also ducking his boss and getting his real work done on the sly. We'll be using Alt-Tab as a working title until we come up with a really badass name.
