﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingBirdController : MonoBehaviour {
    public float speed = 4.0f;
    public float selfDestroyInSec = 3.0f;
    public bool startedFlying = false;

    private MovingPlatformMotor2D _mpMotor;

	// Use this for initialization
	void Start () {
		_mpMotor = GetComponent<MovingPlatformMotor2D>();
	}

    public void StartFlying()
    {
        if (!startedFlying)
        {
            _mpMotor.velocity = -Vector2.right * speed;
            Destroy(gameObject, selfDestroyInSec);
            startedFlying = true;
        }
    }
}
