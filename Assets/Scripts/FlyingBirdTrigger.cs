﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingBirdTrigger : MonoBehaviour {
    public FlyingBirdController targetBird;
    private bool isTouched = false;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "Player")
		{
			if (!isTouched)
			{
				isTouched = true;
                targetBird.StartFlying();
			}
		}
	}
}
