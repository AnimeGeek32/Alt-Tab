﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathVoidController : MonoBehaviour {
    public bool isTouched = false;

	void OnTriggerEnter2D(Collider2D collider)
	{
        if (collider.tag == "Player")
		{
			if (!isTouched)
			{
                isTouched = true;
				collider.gameObject.GetComponent<PlayerController2D>().enabled = false;
                AltTab.GameManager.instance.GameOverLoseAtGame();
			}
		}
	}
}
