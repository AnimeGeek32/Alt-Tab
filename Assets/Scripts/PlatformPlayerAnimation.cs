﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformPlayerAnimation : MonoBehaviour {
	public GameObject visualChild;

    [Header("Sounds")]
    [FMODUnity.EventRef]
    public string playerJumpSoundEvent;
    FMOD.Studio.EventInstance playerJumpSound;
    [FMODUnity.EventRef]
    public string playerLandSoundEvent;
    FMOD.Studio.EventInstance playerLandSound;

    private PlatformerMotor2D _motor;
	private Animator _animator;
	private bool _isJumping;
	private bool _currentFacingLeft;
    private int currentNumOfJumps = 0;
    private int maxNumOfJumps = 2;

	// Use this for initialization
	void Start () {
		_motor = GetComponent<PlatformerMotor2D>();
		_animator = visualChild.GetComponent<Animator>();

        playerJumpSound = FMODUnity.RuntimeManager.CreateInstance(playerJumpSoundEvent);
        currentNumOfJumps = 0;
        maxNumOfJumps = 1 + _motor.numOfAirJumps;

        playerLandSound = FMODUnity.RuntimeManager.CreateInstance(playerLandSoundEvent);

        _motor.onJump += PlayerJumpSfx;
        _motor.onLanded += PlayerLandSfx;
    }
	
	// Update is called once per frame
	void Update () {
		if (_motor.motorState == PlatformerMotor2D.MotorState.Jumping ||
				_isJumping &&
					(_motor.motorState == PlatformerMotor2D.MotorState.Falling ||
								 _motor.motorState == PlatformerMotor2D.MotorState.FallingFast))
		{
			_isJumping = true;
            _animator.SetFloat("VelocityY", _motor.velocity.y);
            _animator.SetBool("InAir", true);

			if (_motor.velocity.x <= -0.1f)
			{
				_currentFacingLeft = true;
			}
			else if (_motor.velocity.x >= 0.1f)
			{
				_currentFacingLeft = false;
			}
        }
		else
		{
			_isJumping = false;
            currentNumOfJumps = 0;

			if (_motor.motorState == PlatformerMotor2D.MotorState.Falling ||
							 _motor.motorState == PlatformerMotor2D.MotorState.FallingFast)
			{
                _animator.SetBool("InAir", true);
                _animator.SetFloat("VelocityY", _motor.velocity.y);
			}
			else
			{
                _animator.SetBool("InAir", false);
                _animator.SetFloat("VelocityY", 0);
                if (Mathf.Abs(_motor.velocity.x) >= 0.1f * 0.1f)
				{
                    _animator.SetFloat("Movement", Mathf.Abs(_motor.velocity.x));
				}
				else
				{
                    _animator.SetFloat("Movement", 0);
				}
			}
		}

		// Facing
		float valueCheck = _motor.normalizedXMovement;

		if (_motor.motorState == PlatformerMotor2D.MotorState.Slipping ||
			_motor.motorState == PlatformerMotor2D.MotorState.Dashing ||
			_motor.motorState == PlatformerMotor2D.MotorState.Jumping)
		{
			valueCheck = _motor.velocity.x;
		}

		if (Mathf.Abs(valueCheck) >= 0.1f)
		{
			Vector3 newScale = visualChild.transform.localScale;
			newScale.x = Mathf.Abs(newScale.x) * ((valueCheck > 0) ? 1.0f : -1.0f);
			visualChild.transform.localScale = newScale;
		}
	}

    void PlayerJumpSfx()
    {
        if (currentNumOfJumps < maxNumOfJumps)
        {
            playerJumpSound.start();
            playerJumpSound.release();
            playerJumpSound = FMODUnity.RuntimeManager.CreateInstance(playerJumpSoundEvent);
            currentNumOfJumps++;
        }
    }

    void PlayerLandSfx()
    {
        playerLandSound.start();
        playerLandSound.release();
        playerLandSound = FMODUnity.RuntimeManager.CreateInstance(playerLandSoundEvent);
    }
}
